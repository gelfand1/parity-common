// Copyright 2020 Parity Technologies
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Parity Codec serialization support for uint and fixed hash.

#[doc(hidden)]
pub use borsh;

/// Add Borsh serialization support to an integer created by `construct_uint!`.
#[macro_export]
macro_rules! impl_uint_borsh {
	($name:ident, $len:expr) => {
		impl $crate::borsh::BorshSerialize for $name {
			fn serialize<W: std::io::Write>(&self, writer: &mut W) -> std::io::Result<()> {
				let mut bytes = [0u8; $len * 8];
				self.to_big_endian(&mut bytes);
				$crate::borsh::BorshSerialize::serialize(&bytes, writer)
			}
		}

		impl $crate::borsh::BorshDeserialize for $name {
			fn deserialize_reader<R: std::io::Read>(reader: &mut R) -> std::io::Result<Self> {
				let b = <[u8; $len * 8] as $crate::borsh::BorshDeserialize>::deserialize_reader(reader)?;
				Ok(b.into())
			}
		}
	};
}

#[macro_export]
macro_rules! impl_fixed_hash_borsh {
	($name: ident, $len: expr) => {
		impl $crate::borsh::BorshSerialize for $name {
			fn serialize<W: std::io::Write>(&self, writer: &mut W) -> std::io::Result<()> {
				$crate::borsh::BorshSerialize::serialize(&self.0, writer)
			}
		}

		impl $crate::borsh::BorshDeserialize for $name {
			fn deserialize_reader<R: std::io::Read>(reader: &mut R) -> std::io::Result<Self> {
				let b = <[u8; $len] as $crate::borsh::BorshDeserialize>::deserialize_reader(reader)?;
				Ok(Self(b))
			}
		}
	};
}
